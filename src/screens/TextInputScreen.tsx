import React from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Text,
} from 'react-native';
import {CustomSwitch} from '../components/CustomSwitch';
import {HeaderTitle} from '../components/HeaderTitle';
import {useForm} from '../hooks/useForm';
import {styles} from '../theme/appTheme';

export const TextInputScreen = () => {
  const {form, onChange, isSubscribed} = useForm({
    name: '',
    email: '',
    phone: '',
    isSubscribed: false,
  });

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}>
      <ScrollView>
        <View style={styles.globalMargin}>
          <HeaderTitle title="TextInput" />

          <TextInput
            style={stylesScreen.inputStyle}
            placeholder="Ingrese su nombre"
            placeholderTextColor="rgba(0,0,0,0.3)"
            autoCorrect={false}
            autoCapitalize="words"
            onChangeText={value => onChange(value, 'name')}
          />
          <TextInput
            style={stylesScreen.inputStyle}
            placeholder="Ingrese su email"
            placeholderTextColor="rgba(0,0,0,0.3)"
            autoCorrect={false}
            autoCapitalize="none"
            onChangeText={value => onChange(value, 'email')}
            keyboardType="email-address"
          />

          <View style={stylesScreen.switchRow}>
            <Text style={stylesScreen.switchText}>Suscribirse:</Text>
            <CustomSwitch
              isOn={isSubscribed}
              onChange={value => onChange(value, 'isSubscribed')}
            />
          </View>

          <HeaderTitle title={JSON.stringify(form, null, 3)} />

          <HeaderTitle title={JSON.stringify(form, null, 3)} />

          <TextInput
            style={stylesScreen.inputStyle}
            placeholder="Ingrese su teléfono"
            placeholderTextColor="rgba(0,0,0,0.3)"
            onChangeText={value => onChange(value, 'phone')}
            keyboardType="phone-pad"
          />
        </View>

        <View style={{height: 50}} />
      </ScrollView>
    </KeyboardAvoidingView>
  );
};

const stylesScreen = StyleSheet.create({
  inputStyle: {
    borderRadius: 5,
    // borderColor: 'grey',
    borderColor: 'rgba(0,0,0,0.3)',
    height: 40,
    margin: 10,
    borderWidth: 1,
    paddingHorizontal: 10,
    color: 'black',
  },
  switchRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignContent: 'center',
    marginVertical: 10,
  },
  switchText: {
    fontSize: 25,
  },
});
