import {MenuItem} from '../interfaces/appInterfaces';

export const menuItems: MenuItem[] = [
  {
    name: 'Animation 101',
    icon: 'cube-outline',
    component: 'Animation101Screen', // Nombre del Screen a navegar
  },
  {
    name: 'Animation 102',
    icon: 'albums-outline',
    component: 'Animation102Screen', // Nombre del Screen a navegar
  },
  {
    name: 'Switches',
    icon: 'toggle-outline',
    component: 'SwitchScreen', // Nombre del Screen a navegar
  },
  {
    name: 'Alerts',
    icon: 'alert-circle-outline',
    component: 'AlertScreen', // Nombre del Screen a navegar
  },
  {
    name: 'TextInputs',
    icon: 'document-text-outline',
    component: 'TextInputScreen', // Nombre del Screen a navegar
  },
  {
    name: 'Pull To Refresh',
    icon: 'refresh-outline',
    component: 'PullToRefreshScreen', // Nombre del Screen a navegar
  },
  {
    name: 'Section List',
    icon: 'list-outline',
    component: 'CustomSectionListScreen', // Nombre del Screen a navegar
  },
  {
    name: 'Modal',
    icon: 'copy-outline',
    component: 'ModalScreen', // Nombre del Screen a navegar
  },
  {
    name: 'InfiniteScroll',
    icon: 'download-outline',
    component: 'InfiniteScrollScreen', // Nombre del Screen a navegar
  },
];
